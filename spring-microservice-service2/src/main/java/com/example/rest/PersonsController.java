package com.example.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class PersonsController {

	@Value("${service.dataBatchSize:0}")
	private int dataBatchSize;

	final Person[] persons = {
			new Person("Tomas", "Kloucek", "Programmer"),
			new Person("Linus", "Torvalds", "Linux"),
			new Person("Heinz", "Kabutz", "Java"),
			new Person("Jonathan", "Locke", "Wicket")
	};

	@RequestMapping("/persons")
    public Persons getPersons() {
    	final Persons result = new Persons();

		for (int i=0; i < dataBatchSize;i++) {
			result.getPersons().add(persons[i]);
		}
    	
    	return result;
    }
}
