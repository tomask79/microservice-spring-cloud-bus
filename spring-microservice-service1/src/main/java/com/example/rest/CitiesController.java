package com.example.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class CitiesController {

	@Value("${service.dataBatchSize:0}")
	private int dataBatchSize;

	final City[] cities = {
			new City("Brno", "Czech republic"),
			new City("Bern", "Switzeland"),
			new City("Berlin", "Germany"),
			new City("London", "England")
	};

	@RequestMapping("/cities")
    public Cities getCities() {
    	final Cities result = new Cities();

		for (int i=0; i < dataBatchSize;i++) {
			result.getCities().add(cities[i]);
		}

    	return result;
    }
}
