# Writing MicroServices [part 7] #

## Spring Cloud Bus ##

When writing MicroServices, soon or later you're going to be solving problem of howto push particular settings or instructions to nodes of your distributed system...Basically I have seen following solutions in the production:

* Using of distributed cache (Hazelcast, Oracle Coherence...)
* Pushing the settings through middle-ware like JMS or AMPQ.

One of options in Spring Cloud for this is to use [Spring Cloud Bus](https://cloud.spring.io/spring-cloud-bus/) which is more or less the second option from my list.
Information transfer between nodes is being done via AMPQ protocol, but they've got other transfers like JMS on the roadmap to support. Anyway, let's stick with AMPQ for now.

### Using of RabbitMQ with Spring Cloud Bus  ###

All you need to do to is:

* add *spring-cloud-starter-bus-amqp* dependency to classpath
* make sure your broker is running.
* If you run it on localhost (testing) you don't need to do anything...it just works. If you're running RabbitMQ separately then following needs to be configured on every node:


```
spring:
  rabbitmq:
    host: <broker host>
    port: <broker port>
    username: <broker user>
    password: <broker password>
```

### Configuration of Spring Cloud Config Server ###

What Spring Cloud Bus offers is sending the instruction to MicroServices listening on the Bus to reload the Spring application properties (it behaves like distributed actuator) via AMPQ messages triggered through following build-in HTTP endpoints:

```
"to invoke the reload of all application.properties of all MicroServices listening on the BUS"
/bus/refresh

"to invoke the reload of application.properties config of specific MicroService"
/bus/refresh?destination=<spring.application.name>:<app port>

"to send a specific key/value pair to MicroService Spring environment"
/bus/env
```
for more information of how to remotely configure MicroServices via Spring Cloud Bus check [Spring Cloud Bus Reference](http://cloud.spring.io/spring-cloud-static/spring-cloud.html#_spring_cloud_bus) Anyway, in distributed system it's good to have some application properties stored at one place and here comes the
[Spring Cloud Config](https://cloud.spring.io/spring-cloud-config/) 

Spring Cloud Config in general is a component that has properties stored in GIT repo. They support many public git portals like GitHub, Bitbucket, GitLab...
If you'd like to use other storage like **database** for storing the properties, here is the answer of the Spring Cloud Config developer:


*Pull requests are welcome. We aren't planning it at the moment. There are also other implementations that don't use config server: spring-cloud-consul and spring-cloud-zookeeper. Some of the reasons we chose git are that it already implements versioning and history, which a db doesn't have by default. Your business may not know git, but in general, most developers do.*

Hmm, let's stick with GIT for now. We will save following property into this GIT repo:

```
service.dataBatchSize=1
```
under the **microservice-spring-cloud-bus/config** folder. To say this to Spring Cloud Config server, you need to configure:

```
spring.cloud.config.server.git.uri=https://tomask79@bitbucket.org/tomask79/microservice-spring-cloud-bus.git
spring.cloud.config.server.git.searchPaths=microservice-spring-cloud-bus, config
```

This configuration pulls the repository under spring.cloud.config.server.git.uri property and tries to locate configuration files under mentioned folder. But under which names? Let's explain, we're going to be using two MicroServices with names **citiesService, personsService** which are going to be listening on the RabbitMQ
Bus. Hence config subfolder will contain files **citiesService.properties and personsService.properties**. **File names maps to spring.application.name properties on the client side**. You can also use spring profiles in the Spring Cloud Config properties, for more information check: [Spring Cloud Config Reference](http://cloud.spring.io/spring-cloud-config/spring-cloud-config.html) 

### Client side usage of Spring Cloud Config Server ### 

Every MicroService needs to have following bootstrap.properties 

```
spring.cloud.config.uri=http://localhost:9999
spring.cloud.config.enabled=true
```

to locate my property **service.dataBatchSize**. 
Let's use it in the following easy MicroService returning cities.

**citiesService:**
```
@RestController
@RefreshScope
public class CitiesController {

	@Value("${service.dataBatchSize:0}")
	private int dataBatchSize;

	final City[] cities = {
			new City("Brno", "Czech republic"),
			new City("Bern", "Switzerland"),
			new City("Berlin", "Germany"),
			new City("London", "England")
	};

	@RequestMapping("/cities")
    public Cities getCities() {
    	final Cities result = new Cities();

		for (int i=0; i < dataBatchSize; i++) {
			result.getCities().add(cities[i]);
		}

    	return result;
    }
}
```

### Testing the whole Spring Cloud Bus application ###

* Start RabbitMQ broker first (start depends on the environment you've got it installed in)
* git clone <this repo>
* mvn clean install (in the root folder with pom.xml)
* cd spring-microservice-registry
* java -jar target/registry-0.0.1-SNAPSHOT.war
* verify that NetFlix Eureka is running at http://localhost:9761
* cd ..
* cd spring-microservice-config
* java -jar target/config-0.0.1-SNAPSHOT.war
* cd ..
* cd spring-microservice-service1
* java -jar target/service1-0.0.1-SNAPSHOT.war
* verify at http://localhost:9761 that citiesService has been registered
* cd ..
* cd spring-microservice-service2
* java -jar target/service2-0.0.1-SNAPSHOT.war
* verify at http://localhost:9761 that personsService has been registered

Everything is running, now hit in the browser:

```
http://localhost:8081/cities
```
You should see cities list with size of equal to "service.dataBatchSize" property.
Now change the property in the citiesService.properties file in the GIT repo. 
If you hit http://localhost:8081/cities again then you won't see any change...
To reload the Spring context of citiesService MicroService run the following:

```
(you can run this at any node having spring-cloud-starter-bus-amqp dependency)
curl -X POST http://localhost:9999/bus/refresh
```

Now hit again http://localhost:8081/cities and property change should be visible...

### Summary ###

Spring Cloud Bus is a complete solution for pushing configuration changes to MicroServices nodes.
If they add another transport ways like JMS it would be more then fine. On the other hand, I don't thing 
it's a problem with creating such solution with pure Spring Integration...It depends on you.